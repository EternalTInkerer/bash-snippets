#!/bin/bash

echo Analyzing $1

# before use check output of fdisk and adjust grep variable to your needs
SS=`fdisk -l $1 | grep Linux`
SS=($SS)  # here we split output of fdisk by space and break it into array
BS=$((SS[1]*512))   # [1] is the index of string value we need
echo Offset = ${BS}

echo mounting to /mnt/sdcard
mkdir -p /mnt/sdcard
mount -o loop,rw,sync,offset=${BS} $1 /mnt/sdcard

ls -l /mnt/sdcard
